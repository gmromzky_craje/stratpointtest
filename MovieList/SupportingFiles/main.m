//
//  main.m
//  MovieList
//
//  Created by Romeo Acuram Jr on 04/05/2018.
//  Copyright © 2018 Romeo J. Acuram Jr. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
