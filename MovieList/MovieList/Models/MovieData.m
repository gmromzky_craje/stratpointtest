#import <Foundation/Foundation.h>
#import "MovieData.h"

static NSString *const MDKeyRating = @"rating";
static NSString *const MDKeyGenres = @"genres";
static NSString *const MDKeyLanguage = @"language";
static NSString *const MDKeyTitle = @"title";
static NSString *const MDKeyUrl = @"url";
static NSString *const MDKeyLongTitle = @"title_long";
static NSString *const MDKeyIMDBCode = @"imdb_code";
static NSString *const MDKeyID = @"id";
static NSString *const MDKeyState = @"state";
static NSString *const MDKeyYear = @"year";
static NSString *const MDKeyRuntime = @"runtime";
static NSString *const MDKeyOverview = @"overview";
static NSString *const MDKeySlug = @"slug";
static NSString *const MDKeyMPARating = @"mpa_rating";

static NSString *const MDImagePrefix = @"https://aacayaco.github.io/movielist/images/";
static NSString *const MDImagePostfixBackdrop = @"-backdrop.jpg";
static NSString *const MDImagePostfixCover = @"-cover.jpg";


@implementation MovieData

- (instancetype)initWithDictionary:(NSDictionary *)dictionary
{
	if (self = [super init]) {
		_rating = [dictionary[MDKeyRating] floatValue];
		_genres = dictionary[MDKeyGenres];
		_language = dictionary[MDKeyLanguage];
		_title = dictionary[MDKeyTitle];
		_url = [NSURL URLWithString:dictionary[MDKeyUrl]];
		_longTitle = dictionary[MDKeyLongTitle];
		_imdbCode = dictionary[MDKeyIMDBCode];
		_movieId = [dictionary[MDKeyID] integerValue];
		_state = dictionary[MDKeyState];
		_year = [NSString stringWithFormat:@"%@",dictionary[MDKeyYear]];
		_runtime = [dictionary[MDKeyRuntime] doubleValue];
		_overview = dictionary[MDKeyOverview];
		_slug = dictionary[MDKeySlug];
		_mpaRating = dictionary[MDKeyMPARating];
	}
	return self;
}

#pragma mark - MovieBasicInformation Datasource

- (NSString *)getShortTitle
{
	return _title;
}

- (NSString *)getYearReleased
{
	return _year;
}

- (NSURL *)getBackdropURL
{
	return [NSURL URLWithString:[NSString stringWithFormat:@"%@%@%@",MDImagePrefix, _slug, MDImagePostfixBackdrop]];
}

- (NSInteger)getMovieId
{
    return _movieId;
}

#pragma mark - MovieDetailedInformation Datasource

- (NSString *)getLongTitle
{
	return _longTitle;
}

- (NSString *)getRating
{
    return [NSString stringWithFormat:@"Rating: %.1f / 10",_rating];
}

- (NSURL *)getCoverURL
{
	return [NSURL URLWithString:[NSString stringWithFormat:@"%@%@%@",MDImagePrefix, _slug, MDImagePostfixCover]];
}

- (NSString *)getGenres
{
	NSString *genres = [_genres firstObject];
	for (int x = 1; x < _genres.count; x++)
	{
		genres = [NSString stringWithFormat:@"%@, %@",genres, _genres[x]];
	}
	return genres;
}

- (NSString *)getLanguage
{
	return _language;
}

- (NSString *)getRuntime
{
	int minutes = (int)_runtime % 60;
	int hours = _runtime / 60;
	
	return [NSString stringWithFormat:@"%dh %dm",hours,minutes];
}

- (NSString *)getOverview
{
	return _overview;
}

- (NSString *)getMPARating
{
	return _mpaRating;
}

@end
