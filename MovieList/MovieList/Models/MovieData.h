#import "MovieBasicInformationDatasource.h"
#import "MovieDetailedInformationDatasource.h"

@interface MovieData : NSObject <MovieBasicInformationDatasource, MovieDetailedInformationDatasource>
@property (assign, readonly, nonatomic) float rating;
@property (copy, readonly, nonatomic) NSArray *genres;
@property (copy, readonly, nonatomic) NSString *language;
@property (copy, readonly, nonatomic) NSString *title;
@property (copy, readonly, nonatomic) NSURL *url;
@property (copy, readonly, nonatomic) NSString *longTitle;
@property (copy, readonly, nonatomic) NSString *imdbCode;
@property (assign, readonly, nonatomic) NSInteger movieId;
@property (copy, readonly, nonatomic) NSString *state;
@property (copy, readonly, nonatomic) NSString *year;
@property (assign, readonly, nonatomic) double runtime;
@property (copy, readonly, nonatomic) NSString *overview;
@property (copy, readonly, nonatomic) NSString *slug;
@property (copy, readonly, nonatomic) NSString *mpaRating;

- (instancetype)initWithDictionary:(NSDictionary *)dictionary;

@end