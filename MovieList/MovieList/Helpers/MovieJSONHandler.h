#import "MovieData.h"

@interface MovieJSONHandler : NSObject

+ (void)downloadMovieJSON:(void (^)(NSArray<MovieData*> *movieList, NSString *errorDescription))completion;

@end