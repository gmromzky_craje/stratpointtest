//
//  ImageHandler.h
//  MovieList
//
//  Created by Romeo Acuram Jr on 05/05/2018.
//  Copyright © 2018 Romeo J. Acuram Jr. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface ImageHandler : NSObject

+ (UIImage *)getBackdropImageWithId:(NSInteger)imageId;
+ (UIImage *)getCoverImageWithId:(NSInteger)imageId;

+ (void)setBackdropImage:(UIImage *)image forId:(NSInteger)imageId;
+ (void)setCoverImage:(UIImage *)image forId:(NSInteger)imageId;
@end
