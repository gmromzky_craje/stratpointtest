//
//  ImageHandler.m
//  MovieList
//
//  Created by Romeo Acuram Jr on 05/05/2018.
//  Copyright © 2018 Romeo J. Acuram Jr. All rights reserved.
//

#import "ImageHandler.h"

@implementation ImageHandler
{
    NSMutableDictionary *_imageDictionary;
}

#pragma mark - Initialization

- (instancetype)init
{
    self = [super init];
    if (self) {
        _imageDictionary = [NSMutableDictionary dictionary];
    }
    return self;
}

+ (ImageHandler *)singleton
{
    static ImageHandler *singleton;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        singleton = [ImageHandler new];
    });
    return singleton;
}

#pragma mark - Public Methods

+ (UIImage *)getBackdropImageWithId:(NSInteger)imageId
{
    return [[self singleton] getBackdropImageWithId:imageId];
}

+ (UIImage *)getCoverImageWithId:(NSInteger)imageId
{
    return [[self singleton] getCoverImageWithId:imageId];
}

+ (void)setBackdropImage:(UIImage *)image forId:(NSInteger)imageId
{
    [[self singleton] setBackdropImage:image forId:imageId];
}

+ (void)setCoverImage:(UIImage *)image forId:(NSInteger)imageId
{
    [[self singleton] setCoverImage:image forId:imageId];
}

#pragma mark - Private Methods

- (UIImage *)getBackdropImageWithId:(NSInteger)imageId
{
    return [_imageDictionary objectForKey:[NSString stringWithFormat:@"%d-backdrop",(int)imageId]];
}

- (UIImage *)getCoverImageWithId:(NSInteger)imageId
{
    return [_imageDictionary objectForKey:[NSString stringWithFormat:@"%d-cover",(int)imageId]];
}

- (void)setBackdropImage:(UIImage *)image forId:(NSInteger)imageId
{
    [_imageDictionary setObject:image forKey:[NSString stringWithFormat:@"%d-backdrop",(int)imageId]];
}

- (void)setCoverImage:(UIImage *)image forId:(NSInteger)imageId
{
    [_imageDictionary setObject:image forKey:[NSString stringWithFormat:@"%d-cover",(int)imageId]];
}

@end
