#import "MovieJSONHandler.h"

static NSString *const MovieJSONURL = @"https://aacayaco.github.io/movielist/list_movies_page1.json";

static NSString *const MovieJSONDataKeyStatus = @"status";
static NSString *const MovieJSONDataKeyStatusMessage = @"status_message";

@implementation MovieJSONHandler

+ (void)downloadMovieJSON:(void (^)(NSArray<MovieData *> *, NSString *))completion
{
    
    dispatch_queue_t background = dispatch_queue_create("DownloadData", NULL);
    dispatch_async(background, ^{
        NSError *error;
        NSData *data = [NSData dataWithContentsOfURL: [NSURL URLWithString:MovieJSONURL]];
        NSDictionary *responseData = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
        dispatch_async(dispatch_get_main_queue(), ^{
            NSString *errorDescription;

            if (!completion) {
                return;
            }

            NSMutableArray *mutableMovies = [NSMutableArray array];
            if ([self isResponseDataValid:responseData errorDescription:&errorDescription]) {
                NSArray *movies = [[responseData objectForKey:@"data"] objectForKey:@"movies"];
                for (NSDictionary *info in movies) {
                    [mutableMovies addObject:[[MovieData alloc] initWithDictionary:info]];
                }
            }

            completion([mutableMovies copy], errorDescription);
        });
    });
}

+ (BOOL)isResponseDataValid:(NSDictionary *)responseData errorDescription:(NSString **)errorDescription
{
	if ([responseData[MovieJSONDataKeyStatus] isEqualToString:@"ok"]) {
		return YES;
	}
	
	*errorDescription = responseData[MovieJSONDataKeyStatusMessage];
	return NO;
}


@end
