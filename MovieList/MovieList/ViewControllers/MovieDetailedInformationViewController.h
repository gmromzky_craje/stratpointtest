//
//  MovieDetailedInformationViewController.h
//  MovieList
//
//  Created by Romeo Acuram Jr on 05/05/2018.
//  Copyright © 2018 Romeo J. Acuram Jr. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MovieDetailedInformationDatasource.h"
#import "MovieBasicInformationDatasource.h"

@interface MovieDetailedInformationViewController : UIViewController
@property (strong, nonatomic) id<MovieBasicInformationDatasource, MovieDetailedInformationDatasource> detail;

@end
