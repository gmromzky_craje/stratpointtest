//
//  MovieBasicInformationTableViewController.m
//  MovieList
//
//  Created by Romeo Acuram Jr on 04/05/2018.
//  Copyright © 2018 Romeo J. Acuram Jr. All rights reserved.
//

#import "MovieBasicInformationTableViewController.h"
#import "MovieJSONHandler.h"
#import "MovieBasicInformationTableViewCell.h"
#import "ImageHandler.h"
#import "MovieDetailedInformationViewController.h"

@interface MovieBasicInformationTableViewController () <UITableViewDataSource,UITableViewDelegate>
@property (copy, nonatomic) NSArray *movieList;
@property (weak, nonatomic) MovieDetailedInformationViewController *detailedViewController;
@end

@implementation MovieBasicInformationTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [MovieJSONHandler downloadMovieJSON:^(NSArray<MovieData *> *movieList, NSString *errorDescription) {
        if (!errorDescription) {
            self.movieList = [movieList copy];
            [self.tableView reloadData];
        } else {
            UIAlertController *ac = [UIAlertController alertControllerWithTitle:errorDescription message:@"Please restart app to try again." preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *ok = [UIAlertAction actionWithTitle:@"Quit" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
                exit(0);
            }];
            [ac addAction:ok];
            [self presentViewController:ac animated:YES completion:nil];
        }
    }];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 100;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.movieList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    MovieBasicInformationTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    id<MovieBasicInformationDatasource> info = [self.movieList objectAtIndex:indexPath.row];
    
    if (cell) {
        cell.title.text = [info getShortTitle];
        cell.year.text = [info getYearReleased];
        cell.backdrop.image = nil;
        
        UIImage *backdrop = [ImageHandler getBackdropImageWithId:[info getMovieId]];
        if (!backdrop) {
            NSURLSessionTask *task = [[NSURLSession sharedSession] dataTaskWithURL:[info getBackdropURL] completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
                if (data) {
                    UIImage *image = [UIImage imageWithData:data];
                    [ImageHandler setBackdropImage:image forId:[info getMovieId]];
                    if (image) {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            MovieBasicInformationTableViewCell *updateCell = (MovieBasicInformationTableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
                            if (updateCell) {
                                updateCell.backdrop.image = image;
                            }
                        });
                    }
                }
            }];
            [task resume];
        } else {
            cell.backdrop.image = backdrop;
        }
        
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    id<MovieDetailedInformationDatasource, MovieBasicInformationDatasource> detail = [self.movieList objectAtIndex:indexPath.row];
    self.detailedViewController.detail = detail;
    
    [_delegate movieBasicInformationTableViewController:self didSelectCellWithDetail:detail];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"BasicToDetailed"]) {
        self.detailedViewController = segue.destinationViewController;
    }
}

@end
