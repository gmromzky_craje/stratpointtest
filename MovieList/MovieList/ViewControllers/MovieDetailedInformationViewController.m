//
//  MovieDetailedInformationViewController.m
//  MovieList
//
//  Created by Romeo Acuram Jr on 05/05/2018.
//  Copyright © 2018 Romeo J. Acuram Jr. All rights reserved.
//

#import "MovieDetailedInformationViewController.h"
#import "ImageHandler.h"

@interface MovieDetailedInformationViewController()
@property (weak, nonatomic) IBOutlet UIImageView *backdrop;
@property (weak, nonatomic) IBOutlet UIImageView *cover;
@property (weak, nonatomic) IBOutlet UILabel *longTitle;
@property (weak, nonatomic) IBOutlet UILabel *year;
@property (weak, nonatomic) IBOutlet UILabel *rating;
@property (weak, nonatomic) IBOutlet UILabel *summary;
@end

@implementation MovieDetailedInformationViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setupUI];
}

- (void)setDetail:(id<MovieBasicInformationDatasource,MovieDetailedInformationDatasource>)detail
{
    if (_detail != detail) {
        _detail = detail;
        [self setupUI];
    }
}

- (void)setupUI
{
    self.title = [_detail getShortTitle];
    self.backdrop.image = [ImageHandler getBackdropImageWithId:[_detail getMovieId]];
    self.longTitle.text = [_detail getLongTitle];
    self.year.text = [_detail getYearReleased];
    self.rating.text = [_detail getRating];
    
    NSString *overview = [_detail getOverview] ? [_detail getOverview] : @"";
    NSString *language = [_detail getLanguage] ? [_detail getLanguage] : @"";
    NSString *genres = [_detail getGenres] ? [_detail getGenres] : @"";
    NSString *mpaRating = [_detail getMPARating] ? [_detail getMPARating] : @"";
    NSString *runtime = [_detail getRuntime] ? [_detail getRuntime] : @"";
    
    self.summary.text = [NSString stringWithFormat:@"Overview: %@\n\nLanguage: %@\n\nGenres: %@\n\nMPA Rating: %@\n\nRuntime: %@",overview, language, genres, mpaRating, runtime];
    
    self.cover.image = nil;
    UIImage *cover = [ImageHandler getCoverImageWithId:[_detail getMovieId]];
    if (!cover) {
        NSURLSessionTask *task = [[NSURLSession sharedSession] dataTaskWithURL:[_detail getCoverURL] completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
            if (data) {
                UIImage *image = [UIImage imageWithData:data];
                [ImageHandler setCoverImage:image forId:[self.detail getMovieId]];
                if (image) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        self.cover.image = image;
                    });
                }
            }
        }];
        [task resume];
    } else {
        self.cover.image = cover;
    }
}

@end
