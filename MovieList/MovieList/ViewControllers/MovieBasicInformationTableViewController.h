//
//  MovieBasicInformationTableViewController.h
//  MovieList
//
//  Created by Romeo Acuram Jr on 04/05/2018.
//  Copyright © 2018 Romeo J. Acuram Jr. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MovieDetailedInformationDatasource.h"
#import "MovieBasicInformationDatasource.h"

@protocol MovieBasicInformationDelegate;
@interface MovieBasicInformationTableViewController : UITableViewController
@property (weak, nonatomic) id<MovieBasicInformationDelegate> delegate;
@end

@protocol MovieBasicInformationDelegate <NSObject>
- (void)movieBasicInformationTableViewController:(MovieBasicInformationTableViewController *)tableViewController didSelectCellWithDetail:(id<MovieBasicInformationDatasource, MovieDetailedInformationDatasource>)detail;
@end
