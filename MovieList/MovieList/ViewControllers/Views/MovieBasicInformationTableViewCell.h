//
//  MovieBasicInformationTableViewCell.h
//  MovieList
//
//  Created by Romeo Acuram Jr on 05/05/2018.
//  Copyright © 2018 Romeo J. Acuram Jr. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MovieBasicInformationDatasource.h"

@interface MovieBasicInformationTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *title;
@property (weak, nonatomic) IBOutlet UILabel *year;
@property (weak, nonatomic) IBOutlet UIImageView *backdrop;

@end
