//
//  MasterViewController.m
//  MovieList
//
//  Created by Romeo Acuram Jr on 05/05/2018.
//  Copyright © 2018 Romeo J. Acuram Jr. All rights reserved.
//

#import "MasterViewController.h"
#import "MovieBasicInformationTableViewController.h"
#import "MovieDetailedInformationViewController.h"

@interface MasterViewController () <MovieBasicInformationDelegate>
@property (weak, nonatomic) MovieBasicInformationTableViewController *basicInfoTableViewController;
@property (weak, nonatomic) MovieDetailedInformationViewController *detailedInfoViewController;

@end

@implementation MasterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.basicInfoTableViewController = self.viewControllers.firstObject.childViewControllers.firstObject;
    self.basicInfoTableViewController.delegate = self;
    self.detailedInfoViewController = self.viewControllers.lastObject;
}

- (void)movieBasicInformationTableViewController:(MovieBasicInformationTableViewController *)tableViewController didSelectCellWithDetail:(id<MovieBasicInformationDatasource, MovieDetailedInformationDatasource>)detail
{
    self.detailedInfoViewController.detail = detail;
}

@end
