#import <Foundation/Foundation.h>

@protocol MovieBasicInformationDatasource <NSObject>
- (NSString *)getShortTitle;
- (NSString *)getYearReleased;
- (NSURL *)getBackdropURL;
- (NSInteger)getMovieId;
@end
