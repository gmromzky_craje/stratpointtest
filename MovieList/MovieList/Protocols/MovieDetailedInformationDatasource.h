#import <Foundation/Foundation.h>

@protocol MovieDetailedInformationDatasource <NSObject>
- (NSString *)getLongTitle;
- (NSString *)getRating;
- (NSURL *)getCoverURL;
- (NSString *)getGenres;
- (NSString *)getLanguage;
- (NSString *)getRuntime;
- (NSString *)getOverview;
- (NSString *)getMPARating;
@end
