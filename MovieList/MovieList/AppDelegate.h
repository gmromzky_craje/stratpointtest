//
//  AppDelegate.h
//  MovieList
//
//  Created by Romeo Acuram Jr on 04/05/2018.
//  Copyright © 2018 Romeo J. Acuram Jr. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

